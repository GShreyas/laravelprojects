$(document).ready(function(){
	//for all the categories
	$.ajax({
			type:"GET",
			url: "get-categories",
			data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json",                    
            cache: false,                       
           	success: function(response) 
				{
					//console.log(response);
					var catList = '';
					$.each(response, function(i, category)
						{ 	
							catList += "<li class='nav-item'><a class='nav-link' href='#' id='category-"+category.cy_id+"' onclick='retrieveProducts(" +category.cy_id+ ")' > "+category.cy_name+ "</a></li>";
						});
					//console.log(catList);	
					$('#category-list').append(catList);
				},
				error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
	//for all the products		
	$.ajax({
			type: "GET",
			url: "get-products",
			data: {},
           	contentType: "application/json; charset=utf-8",
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allProducts = '';
				$.each(response, function(i, product)
					{ 	
						allProducts += "<div class='col'><div class='card h-100 text-center'><img src='" +product.pr_img+
						"' class='card-img-top' alt='" +product.pr_name+ "'><div class='card-body'><h6 class='card-title'>" +product.pr_name+ "</h6><a href='#' id='"+product.pr_id+
						"' name='submit' class='btn btn-primary btn-block' >View</a></div></div></div>"
					});
				//console.log(allProducts);
				$('#product-list').html(allProducts);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
});

function retrieveProducts(catID){
	$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				var sendData = {'cy_id': catID};
	$.ajax({
			type: "POST",
			url: "get-category-products",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allProducts = '';
				$.each(response, function(i, product)
					{ 	
						allProducts += "<div class='col'><div class='card h-100 text-center'><img src='" +product.pr_img+
						"' class='card-img-top' alt='" +product.pr_name+ "'><div class='card-body'><h6 class='card-title'>" +product.pr_name+ "</h6><a href='#' id='"+product.pr_id+
						"' name='submit' class='btn btn-primary btn-block' >View</a></div></div></div>"
					});
				//console.log(allProducts);
				$('#product-list').html(allProducts);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

	