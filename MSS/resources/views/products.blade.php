@include('header')
@include('navbar')
<div class="container-fluid">
	<div class="row">
		<div class="col-md w-100 text-center">
			<img src="{{ asset('images/leads.gif') }}" class="img-fluid">
			<h1 class="mt-5">Products</h1>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<h6>Filter by Category</h6>
			<nav class="navbar">
				<ul class="navbar-nav" id="category-list">
	
				</ul>
			</nav>
		</div>
		<div class="col-md-9">
			<div class="row  row-cols-1 row-cols-md-3 g-4" id="product-list">
			
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center mt-5" style="color:#529BFF">Get In Touch With Us</h2>
			<button class="btn btn-lg d-grid mx-auto my-5"  type="button" style="color:#FFFFFF;background-color:#529BFF">CONTACT US</button>
		</div>
	</div>
</div>
@include('footer-bar')
@include('footer')