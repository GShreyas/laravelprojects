<div class="container-fluid">
	<div class="row d-flex ">
		<div class="col">
			<a class="navbar-brand" href="index">
				<img class="d-block mx-auto mt-2" src="{{ asset('images/logo.png')	 }}">
			</a>
		</div>	
	</div>
</div>
<nav class="container-fluid navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
   </button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mx-auto">
			<li class="nav-item">
				<a class="nav-link" href="index">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="aboutus">About Us</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="products">Products</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="contactus">Contact Us</a>
			</li>
		</ul>
	</div>
</nav>



