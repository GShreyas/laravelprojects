<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\FormsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','index');

Route::view('/index','index');
Route::view('/aboutus','aboutus');
Route::view('/products','products');
Route::view('/contactus','contactus');

Route::get('get-categories',[CategoryController::class, 'getCategories']);//get all categories-ajax

Route::get('get-products',[ProductController::class, 'allProducts']);//get all products-ajax
Route::post('get-category-products',[ProductController::class, 'catProducts'])->name('get-category-products.post');//get products as per category-ajax

Route::post('submit-form',[FormsController::class, 'insert'])->name('submit-form.post');//form submission-ajax
