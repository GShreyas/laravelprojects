@include ('header')
@include ('navbar')
<!--------------------------------------------------Hero---------------------------------------------------------->
<section>
	<div class="container-fluid products-cover shadow">
		<div class="row">
		 <div class="col-sm-12">
			<h1 class="text-white fw-bold ms-1 mt-3">
				Products
			</h1>
		 </div>
		</div>
	</div>	
</section>
<!------------------------------------------------------------------------------------------------------------>
<aside>
	<div class="container-fluid" >
		<div class="row ">
			<div class="col-md-3 pt-3 shadow" id="cat_bar">
				<h2 class="ms-3 mt-3 fw-bold fs-4">Filter by categories</h2>
				<ul class="nav flex-column" id="category-list">

				</ul>
			</div>
			<div class="col-md-9 pt-3 pe-4 " id="pro_bar">
				<h3 id="catName" class="mt-3 fw-bold fs-4"></h3>
				<div class="row  row-cols-1 row-cols-md-3 g-4 py-2" id="subCategory-list">
			
				</div>
				<div class="row" id="varFill" style="display:none">
					<img src="images/logo.png" alt="">
				</div>
			</div>
		</div>
	</div>
</aside>
<script>
$(document).ready(function(){
	$.ajaxSetup({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});

    //Display category name on opening the page for the first time
    //To display subcategories of the first category when the products page is opened for the first time.
        retrieveSubCat(1,"Solar Charge Controller");
});

//To display sub categories when user selects a category.
function retrieveSubCat(catID,cyName){
    $('#varFill').hide();
	$('#subCategory-list').show();
	var sendData = {'cy_id': catID};
    var catName = cyName;
	$.ajax({
			type: "POST",
			url: "get-category-subCat",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allsubCat = '';
				

				$.each(response, function(i, subCat)
					{ 	
						allsubCat += "<div class='col'><div class='card h-100 text-center shadow hover-card'><img src='" +subCat.sub_img+
							"' class='card-img-top pro-img mx-auto' alt='" +subCat.sub_name+ "'><div class='card-body'><h6 class='card-title fw-bold fs-5'>" +subCat.sub_name+ "</h6><a href='#' class='btn btn-primary fw-bold' onclick='showProd(\"" +subCat.PK_sub_id+ "\",\"" +subCat.sub_name + "\"); return false'>View</a></div></div></div>";

					});
				//console.log(allProducts);
				$('#subCategory-list').html(allsubCat);
                $('#catName').html(catName);
				
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

//To display products when user selects a sub category
function showProd(subID,subName){
	$('#varFill').hide();
	$('#subCategory-list').show();
	var sendData = {'sub_id': subID};
	var subName = subName;
	$.ajax({
			type: "POST",
			url: "get-subcategory-products",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var subProducts = '';
				$.each(response, function(i, product)
					{ 	
						subProducts += "<div class='col'><div class='card h-100 text-center shadow hover-card'><img src='" +product.pr_img+
						"' class='card-img-top pro-img mx-auto' alt='" +product.pr_name+ "'><div class='card-body'><h6 class='card-title fw-bold fs-5'>" +product.pr_name+ "</h6><a href='#' class='btn btn-primary fw-bold mt-2' onclick='showVar("+product.PK_pr_id+");return false'>View</a></div></div></div>";

					});
				//console.log(allProducts);
				$('#subCategory-list').html(subProducts);
				$('#catName').append(" - "+subName);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});	
};
// To display variants - on clicking view product
function showVar(prID){
	$('#varFill').hide();
	$('#subCategory-list').show();
    var sendData = {'prID':prID};
    $.ajax({
			type: "POST",
			url: "get-product-var",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var variants = '';
				$.each(response, function(i, variant)
					{ 	
						variants += "<div class='col'><div class='card h-100 text-center shadow hover-card'><img src='"+variant.var_img+"' class='card-img-top pro-img mx-auto' alt=''><div class='card-body'><h5 class='card-title fw-bold fs-5'>" +variant.var_details+ "</h5><h6 class='card-subtitle mb-2 text-muted'>"+variant.var_mod_no+"</h6><div class='card-footer'><a href='#' class='btn btn-primary fw-bold mt-2' onclick='disVar(\"" +variant.PK_var_id+ "\",\"" +variant.var_mod_no+ "\");return false' >View</a></div></div></div></div>";

					});
				//console.log(variants);
				$('#subCategory-list').html(variants);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});

};

function disVar(varID,varMod){
	$('#subCategory-list').hide();
	$('#varFill').show();
    var sendData = {'varID':varID,'varMod':varMod};
	//console.log(sendData);
    $.ajax({
			type: "POST",
			url: "get-var-details",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var varDetails = '';
				$.each(response, function(i, variant)
					{ 	
                        //varDetails += ;
					});
				//console.log(varDetails);
				//$('#subCategory-list').html(variants);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
};
</script>
@include ('footerbar')
@include ('footer')
