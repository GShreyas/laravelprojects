@include ('header')
@include ('navbar')
<!--------------------------------------------------Hero---------------------------------------------------------->
<section>
	<div class="container-fluid products-cover shadow">
		<div class="row">
		 <div class="col-sm-12">
			<h1 class="text-white fw-bold ms-1 mt-3">
				Products
			</h1>
		 </div>
		</div>
	</div>	
</section>
<!------------------------------------------------------------------------------------------------------------>

<!--Variants Modal-->
	<div class="modal fade" id="varModal" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg modal-dialog-scrollable">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="ModalLabel"></h5>
			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		  </div>
		  <div class="modal-body">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 card" id="modalImage">
							
					</div>
					<div class="col-md-6 py-6">
						<div class="d-grid gap-2 col-6 mx-auto " id="proBtn">

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" id='modalDesc'>
							
					</div>
				</div>
				
				<div class="row card">
					<table class='table table-hover table-primary'>
						<thead class="card-header">
							<tr>
								<th scope='col'>Model No.</th>
								<th scope='col' >Details</th>
								<th scope='col'>Enquire</th>
							</tr>
						</thead>
						<tbody id="bodyTable" class="fw-bold">
						
						</tbody>
					</table>
				</div>
			</div>
		  </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			</div>
			</div>
    	</div>
	</div>
	
<!-- Enquiry Modal -->

<div class="modal fade" id="EnquiryModal" tabindex="-1" aria-labelledby="EnquiryModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="EnquiryModalLabel">
		
		</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        
		<form method="post" id="enqForm"> 
		@csrf
		  <div class="input-group visually-hidden" id="hide_inp">
			
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon1">Name</span>
			  <input type="text" class="form-control" placeholder="Enter Name" aria-label="Name" aria-describedby="basic-addon1" id="name" name="name" autofocus required/>
			  <span class="text-danger error-text name_err"></span>
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon2">Number</span>
			  <input type="text" class="form-control" placeholder="Enter Number" aria-label="Number" aria-describedby="basic-addon2" id="num" name="number" required/>
			  <span class="text-danger error-text num_err"></span>
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon4">Alternate Number</span>
			  <input type="text" class="form-control" placeholder="Enter Alternate Number" aria-label="AlternateNumber" aria-describedby="basic-addon4" id="altNum" name="altNumber" required/>
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon3">E-mail</span>
			  <input type="mail" class="form-control" placeholder="Enter E-mail" aria-label="E-mail" aria-describedby="basic-addon3" id="email" name="mail" required/>
			  <span class="text-danger error-text email_err"></span>
			  
		 </div>
		 <div class="input-group">
			  <span class="input-group-text">Enquiry</span>
			  <textarea class="form-control" aria-label="With textarea" id="enq" name="enquiry" required></textarea>
			  <span class="text-danger error-text enq_err"></span>
		 </div>
		  
		</form>
		
      </div>
      <div class="modal-footer">
		<button class="btn btn-primary" type="submit" id="formSubmit">Send via Email</button>
        <a class="btn btn-success" href="#" target='_blank'  role="button" id="wp-button">Send via Whatsapp</a>
      </div>
    </div>
  </div>
</div>
	<!---->
<aside>
	<div class="container-fluid" >
		<div class="row ">
			<div class="col-md-3 pt-3 shadow" id="cat_bar">
				<h2 class="ms-3 mt-3 fw-bold fs-4">Filter by categories</h2>
				<ul class="nav flex-column" id="category-list">

				</ul>
			</div>
			<div class="col-md-9 pt-3 pe-4 " id="pro_bar">
				<h3 id="catName" class="mt-3 fw-bold fs-4"></h3>
				<div class="row  row-cols-1 row-cols-md-3 g-4 py-2" id="subCategory-list">
			
				</div>
			</div>
		</div>
	</div>
</aside>
<script>
$(document).ready(function(){
	$.ajaxSetup({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});

//Form validation

//Displaying all the categories in the vertical navbar
	$.ajax({
			type:"POST",
			url: "get-categories",
			data: {},
            dataType: "json",                    
            cache: false,                       
           	success: function(response) 
				{
					//console.log(response);
					var catList = '';
					var footerProdList ='';
					$.each(response, function(i, category)
						{ 	   

							catList += "<li class='nav-item'><a class='nav-link fw-bold' href='#' id='category-"+category.PK_cy_id+"' onclick='retrieveSubCat(\"" +category.PK_cy_id+ "\",\"" +category.cy_name + "\"); return false' > "+category.cy_name+ "</a></li>";
                            
						});
					//console.log(catList);	
					$('#category-list').append(catList);
				
				},
				error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
		});
 //Display category name on opening the page for the first time

    var sData = {'cy_id': 1};
 $.ajax({
			type:"POST",
			url: "get-cat-name",
			data: sData,
            dataType: "json",                    
            cache: false,                       
           	success: function(response) 
				{
					//console.log('response-'+response);
					
					var catName='';
					$.each(response, function(i, category)
						{ 	
							catName = category.cy_name; 
						});
					//console.log('catName-'+catName);	
					$('#catName').html(catName);
				},
				error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
		});       
	
//To display subcategories of the first category when the products page is opened for the first time.
	var sendData = {'cy_id': 1};
	$.ajax({
			type: "POST",
			url: "get-category-subCat",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allsubCat = '';

				$.each(response, function(i, subCat)
					{ 	
						allsubCat += "<div class='col'><div class='card shadow hover-card'><img src='" +subCat.sub_img+
							"' class='card-img-top pro-img mx-auto' alt='" +subCat.sub_name+ "'><div class='card-body text-center'><h6 class='card-title fw-bold fs-5'>" +subCat.sub_name+ "</h6><a href='#' class='btn btn-primary fw-bold mt-2' onclick='showProd(\"" +subCat.PK_sub_id+ "\",\"" +subCat.sub_name + "\"); return false'>View</a></div></div></div>";
						
					});
				//console.log(allProducts);
				$('#subCategory-list').html(allsubCat);
				
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
//For enquiry form submit to DB	
	$('#formSubmit').on('click',(function(m){
			m.preventDefault();
			//alert("click...");
		 
			var name	=	$('#name').val();	
				//console.log(name);
	    	var num		=	$('#num').val();
				//console.log(num);
			var altNum	=	$('#altNum').val();
				//console.log(altNum);
			var email	=	$('#email').val();
				//console.log(email);
			var enq		=	$('#enq').val();
				//console.log(enq);
			var vr_id	= $('#var_id').val();
				//console.log(vr_id);
			var var_mod	= $('#var_mod').val();
				//console.log(var_mod);	
				
			var formData = {'name':name,'num':num,'altNum':altNum,'email':email,'enq':enq,'vr_id':vr_id, 'var_mod':var_mod};
				//console.log(formData);
		  $.ajax({
				  type		:'POST',
				  url		:'submit-form-db',
				  data		:formData,
				  dataType	:'json',                    
				  cache		:false,                       
				  success	:function(response) {
					//console.log("--------1111111111111111111----------");
					//console.log(response.error);
                    if($.isEmptyObject(response.error))
						{	
							//console.log("------------22222222222222--------------");
							//console.log(response.success);
                        	alert(response.success);
                    	}
					else
						{
							//console.log("------------33333333333333333333333--------------");
                        	printErrorMsg(response.error);
							//console.log(response.error);
						}
				  },
				  error: function(f){
						alert('Enquiry form DB AJAX Error!');
						console.log('AJAX Error!');
						console.log(f);
					},		
		  });

		  function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            //console.log(key);
              $('.'+key+'_err').text(value);
            });
        	}
	  })); 
//For enquiry form submit to Mail
	/* $('#formSubmit').on('click',(function(){
          
		 
			var name	=	$('#name').val();	
				//console.log(name);
	    	var num		=	$('#num').val();
				//console.log(num);
			var altNum	=	$('#altNum').val();
				//console.log(altNum);
			var email	=	$('#email').val();
				//console.log(email);
			var enq	=	$('#enq').val();
				//console.log(enq);
			var vr_id = $('#var_id').val();
				//console.log(vr_id);
			var var_mod= $('#var_mod').val();
				//console.log(var_mod);	

			var formData = {'name':name,'num':num,'altNum':altNum,'email':email,'enq':enq,'vr_id':vr_id, 'var_mod':var_mod};
				//console.log(formData);
		  $.ajax({
				  type		:'POST',
				  url		:'submit-form-email',
				  data		:formData,
				  dataType	:'json',                    
				  cache		:false,                       
				  success	:function(response) {
					  alert("Success");
				  },
				  error: function(f){
						alert('Enquiry form Email AJAX Error!');
						console.log('AJAX Error!');
						console.log(f);
					},		
		  });
	  }));  */
});

//To display sub categories when user selects a category.
function retrieveSubCat(catID,cyName){
    
	var sendData = {'cy_id': catID};
    var catName = cyName;
	$.ajax({
			type: "POST",
			url: "get-category-subCat",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allsubCat = '';
				

				$.each(response, function(i, subCat)
					{ 	
						allsubCat += "<div class='col'><div class='card h-100 text-center shadow hover-card'><img src='" +subCat.sub_img+
							"' class='card-img-top pro-img mx-auto' alt='" +subCat.sub_name+ "'><div class='card-body'><h6 class='card-title fw-bold fs-5'>" +subCat.sub_name+ "</h6><a href='#' class='btn btn-primary fw-bold' onclick='showProd(\"" +subCat.PK_sub_id+ "\",\"" +subCat.sub_name + "\"); return false'>View</a></div></div></div>";

					});
				//console.log(allProducts);
				$('#subCategory-list').html(allsubCat);
                $('#catName').html(catName);
				
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

//To display products when user selects a sub category
function showProd(subID,subName){
	
	var sendData = {'sub_id': subID};
	var subName = subName;
	$.ajax({
			type: "POST",
			url: "get-subcategory-products",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var subProducts = '';
				$.each(response, function(i, product)
					{ 	
						subProducts += "<div class='col'><div class='card h-100 text-center shadow hover-card'><img src='" +product.pr_img+
						"' class='card-img-top pro-img mx-auto' alt='" +product.pr_name+ "'><div class='card-body'><h6 class='card-title fw-bold fs-5'>" +product.pr_name+ "</h6><a href='#' class='btn btn-primary fw-bold mt-2' data-bs-toggle='modal' data-bs-target='#varModal' onclick='varModal("+product.PK_pr_id+")'>View</a></div></div></div>";

					});
				//console.log(allProducts);
				$('#subCategory-list').html(subProducts);
				$('#catName').append(" - "+subName);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

//To display product variant details in the modal when user clicks on a product.

function varModal(prID)
{
	var sendData = {'sub_id': prID};
	$.ajax({
			type: "POST",
			url: "get-product-var",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var prodVar = '';
				$.each(response, function(i, variant)
					{ 	
						prodVar += "<tr><td>"+variant.var_mod_no+"</td><td>"+variant.var_details+"</td><td><a href='#' class='btn btn-danger' id='enq-btn' data-bs-toggle='modal' data-bs-target='#EnquiryModal' data-bs-dismiss='modal' onclick='enqModal("+variant.PK_var_id+")'>Enquire</a></td></tr>"
						
					});
				//console.log(allProducts);
				$('#bodyTable').html(prodVar);
				//$('#ModalLabel').html();
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
			
	$.ajax({
			type: "POST",
			url: "get-prod-modal",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var imagesModal = '';
				var descModal = '';
				var proBtn = '';
				var modalLbl = '';

				$.each(response, function(i, product)
					{ 	
						imagesModal += "<img src='"+product.pr_img+"' class='img-fluid'>";

						modalLbl += product.pr_name;

						proBtn += "<a class='btn btn-primary fw-bold' target='_blank' href='"+product.pr_ds+"'>Data Sheet</a><a class='btn btn-primary fw-bold ' target='_blank' href='"+product.pr_man+"'>User Manual</a>";

						descModal += "<h4 class=' fw-bold'>Product Description</h4><p class=''>"+product.pr_desc+"</p>";
					});
				//console.log(allProducts);
				$('#modalImage').html(imagesModal);
				$('#proBtn').html(proBtn);
				$('#modalDesc').html(descModal);
				$('#ModalLabel').html(modalLbl);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},		
			});			
			
			
}

function enqModal(vrID)
{
	var sendData ={'vr_id': vrID};
	$.ajax({
			type: "POST",
			url: "get-var-mod",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var prodVar = '';
				var hide_inp='';
				$.each(response, function(i, variant)
					{ 	
						prodVar += "<span>"+variant.var_mod_no+" - </span><span>"+variant.var_details+"</span>"
						hide_inp += "<input type='text' class='form-control' id='var_id' value='"+variant.PK_var_id+"' disabled><input type='text' class='form-control' id='var_mod' value='"+variant.var_mod_no+"-"+variant.var_details+"' disabled>"
					});
				//console.log(allProducts);
				$('#EnquiryModalLabel').html(prodVar);
				$('#hide_inp').html(hide_inp);
				//$('#ModalLabel').html();
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
	
}

/* $('#wp-button').on('click',(function(){
          //e.preventDefault();
		 
			var name	=	$('#name').val();	
				//console.log(name);
	    	var num		=	$('#num').val();
				//console.log(num);
			var altNum	=	$('#altNum').val();
				//console.log(altNum);
			var email	=	$('#email').val();
				//console.log(email);
			var enq	=	$('#enq').val();
				//console.log(enq);
			var vr_id = $('#var_id').val();
				//console.log(vr_id);
			var var_mod= $('#var_mod').val();
				//console.log(var_mod);	

            var formData = name.concat(num,"||",altNum,"||",email,"||",enq,"||",vr_id,"||",var_mod);
              window.location.href = 'https://api.whatsapp.com/send?phone=+917899149177&text='+formData; 
              
})); */
</script>
@include ('footerbar')
@include ('footer')
