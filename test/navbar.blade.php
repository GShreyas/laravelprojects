
<!-----------------------------------------------------Navigation Bar---------------------------------------------------------------->
   	<header>
		<nav class="navbar navbar-light border-bottom navbar-expand-lg bg-light ">
			<div class="container-fluid">
				<a class="navbar-brand brand fs-2 ms-md-2" href="#">
				  <img src="{{ asset('images/logo.png') }}" alt="Logo">
				</a>
				<button class="navbar-toggler border-0 p-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav d-flex align-items-xxl-center ms-md-3">
						<li class="nav-item">
						  <a class="nav-link active text-black fw-bold" aria-current="page" href="index">Home</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link fw-bold" aria-current="page" href="aboutus">About us</a>
						</li>
						<!--li class="nav-item">
						  <a class="nav-link fw-bold" aria-current="page" href="#">Services</a>
						</li-->
						<li class="nav-item">
						  <a class="nav-link fw-bold" aria-current="page" href="products">Products</a>
						</li>
						<!--li class="nav-item">
						  <a class="nav-link fw-bold" aria-current="page" href="#">Clients</a>
						</li-->
						<li class="nav-item">
						  <a class="nav-link fw-bold" aria-current="page" href="contactus">Contact us</a>
						</li>
						<li class="nav-item d-xxl-none d-md-none">
						  <a class="nav-link disabled fw-bold" aria-current="page" href="#"><span class="span-yellow">+91 1234567890</span></a>
						<li>
						<li class="nav-item d-xxl-none d-md-none">
						  <a class="nav-link disabled fw-bold" aria-current="page" href="#"><span class="span-yellow">info@maruthisolar.com</span></a>
						<li>
					</ul>
				</div>
				<div class="float-end d-none d-md-none d-lg-block d-xl-block">
					<div class="d-block fw-bold"><span >Call Us :</span><span class="span-yellow"> +91 1234567890</span></div>
					<div class="d-block me-4 fw-bold"><span class="span-yellow">info@maruthisolar.com</span></div>
				</div>	
			</div>
		</nav>
	</header>