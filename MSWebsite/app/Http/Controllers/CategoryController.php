<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Category;

class CategoryController extends Controller
{
    public function getAllCat(){
		//DB::enableQueryLog(); // Enable query log
		//$qcatSelAll=Category::all();
		$qcatSelAll=DB::table('categories')
            ->select('PK_cy_id', 'cy_name','cy_img')
            ->get();
		//\Log::debug(DB::getQueryLog());
		//\Log::info($qcatSelAll);
		return response()->json($qcatSelAll);
		
	}

	public function getSpeCat(Request $request)
	{
		$input = $request->all();
		//\Log::info('-input----------------------------------------');
		//\Log::info($input);
		//\Log::info('-input----------------------------------------');
		$cat=Category::select("cy_name")
			->where("PK_cy_id","=",$input)
			->get();
		//\Log::info('--cat---------------------------------------');
		//\Log::info($cat);
		return response()->json($cat);

	}
}
