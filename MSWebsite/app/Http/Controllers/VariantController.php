<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Variant;
use DB;

class VariantController extends Controller
{
    //
	public function getSpeProdVar(Request $prID)
	{
		$input=$prID->all();
		$variants=Variant::select("*")
			->where("FK_pr_id","=",$input)
			->get();
		//\Log::info($variants);
		return response()->json($variants);
	}
	
	public function getVarNo(Request $varID)
	{
		$input=$varID->all();
		$variants=Variant::select("*")
			->where("PK_var_id","=",$input)
			->get();
		//\Log::info($variants);
		return response()->json($variants);
	}

	public function getVarDet(Request $req)
	{	
		$vid = $req->input('varID');
		$vmod = $req->input('varMod');
		$query = DB::select(DB::raw("SELECT * FROM(SELECT variants.PK_var_id,variants.var_mod_no,variants.var_details,variants.var_img,variants.vr_ds,subcategories.sub_desc,subcategories.sub_feat,subcategories.sub_um
													FROM variants 
													INNER JOIN products 
													ON variants.FK_pr_id = products.PK_pr_id
													inner join subcategories 
													on products.FK_sub_id = subcategories.PK_sub_id)
													AS newTable
													WHERE var_mod_no = '$vmod';"));
		//\Log::info($query);
		return response()->json($query);			

	}
}
