<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Variant;

class Enquiry extends Model
{
    //use HasFactory;
	
	public class variant()
	{
		return $this->belongsTo(Variant::class);
	}
}
