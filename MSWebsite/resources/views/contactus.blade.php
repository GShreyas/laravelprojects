@include ('header')
@include ('navbar')
<section>
    <div class="container-fluid flexing py-5" >
        <div class="row shadow border-3 p-4" style=" border: 2px solid red">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12 p-3">
                        <span class="fs-5">Contact Details</span>
                    </div>    
                </div>
                <div class="row mt-2 mb-3">
                    <div class="col-md-3 flexing px-0">
                        <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-9 px-0">
                        <span class="d-block fw-bold fs-6">Contact Person</span>
                        <span class="d-block font-14">Mr. ABC</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 d-flex justify-content-center px-0">
                        <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-9 px-0">
                       <!--a href="https://goo.gl/maps/97rvSHShNzoQrmtH6" target="base"-->
                       <span class="fw-bold fs-6">Address :</span>
                       <span class="float-end fw-bold fs-6"><i class="fa fa-map m-1" aria-hidden="true"></i>Get directions</span>
                       <address class="pt-2 font-14">
                            <span class="span-yellow fw-bold d-block pb-1">Maruthi Solar Systems</span>
                            # 41,42,43, Sy. No. 10/1<br>
                            Abbigere Main Road<br>
                            Near Bus Stop<br>
                            Kereguddadahalli<br>
                            Bangalore - 560090
                        </address>
                        <!--/a-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <i class="fa fa-phone fa-3x" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-9">
                        <abbr title="Phone">P:</abbr><a href="tel:+917899149177">+91 7899149177</a></br>
                        <abbr title="Whatsapp">WP:</abbr><a href="https://api.whatsapp.com/send?phone=+917899149177&text=Hi">+91 7899149177</a></br>
                        <abbr title="E-mail">E:</abbr><a href="mailto:someone@example.com"> info@maruthisolar.com</a>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <div class="line1"></div>
            </div>
            <div class="col-md-7">
                <h6>Enquiry Form</h6>
                <form class="">
					<div class="row">
					  <div class="col-md-12">
						<textarea class="form-control mb-3" id="exampleFormControlTextarea1"  rows="3" placeholder="Describe your requirements"></textarea>
					  </div>
					</div>
					<div class="row">
					  <div class="col-md-12">
						<input type="text" class="form-control" placeholder="Enter your name" aria-label="First name">
					  </div>
                    </div>
                    <div class="row">  
					  <div class="col-md-12">
						<input type="text" class="form-control" placeholder="Enter your number" aria-label="Last name">
					  </div>
					</div>
					<div class="row">
						<div class="col-md-12 text-center">
							<a href="#" class="btn btn-block border-0 hero-button my-3">Send Enquiry</a>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
</section>
@include ('footerbar')
@include ('footer')
