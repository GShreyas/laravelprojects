<!-----------------------------------------------------Footer----------------------------------------------------->
	<footer class="footer">
		<div class="container-fluid px-6" id="footer-cont">
			<div class="row  pt-4">
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-12">
							<a class="brand fs-2" href="#">
								<img src="images/logo.png" alt="">
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-2 mt-2 mt-md-0 " id="footerProdList">
					<h6 class="span-yellow fw-bold">Products</h6>
				</div>
				<div class="col-md-2 mt-2 mt-md-0 ">
					<h6 class="span-yellow fw-bold">Explore</h6>
					<h6><a href="aboutus" class="text-deco">About Us</a></h6>
					<h6>Products</h6>
					<h6>Services</h6>
					<h6>Clients</h6>
					<h6>Contact Us</h6>
				</div>
				<div class="col-md-3 mt-2 mt-md-0 ">
					<address>
						<h6 class="span-yellow fw-bold">Maruthi Solar Systems</h6>
						# 41,42,43, Sy. No. 10/1<br>
						Abbigere Main Road<br>
						Near Bus Stop,<br>
						Kereguddadahalli<br>
						Bangalore - 560090.<br>
						<abbr title="Phone">P:</abbr> +91-80-23253889<br>
						<abbr title="E-mail">E:</abbr> info@maruthisolar.com
					</address>
				</div>
				<div class="col-md-2 mt-2 mt-md-0 ">
					<div class="row">
						<div class="col-md-12">
							<h6><span class="span-yellow fw-bold">Next : </span><a href="aboutus" class="text-deco">About Us</a></h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script>
$(document).ready(function(){
	$.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
    //Displaying all the categories in the footer			
	//Displaying all the categories in the vertical navbar - Products page
	$.ajax({
			type:"POST",
			url: "get-categories",
			data: {},
			dataType: "json",                    
			cache: false,                       
			success: function(response) 
				{
					//console.log(response);
						
					var footerProdList ='';
					var catList = '';
					$.each(response, function(i, category)
						{ 	   	
							footerProdList += "<h6>"+category.cy_name +"</h6>";
							catList += "<li class='nav-item catLi' id='hlt'><a class='nav-link fw-bold catLink' href='#' id='category-"+category.PK_cy_id+"' onclick='retrieveSubCat(\"" +category.PK_cy_id+ "\",\"" +category.cy_name + "\"); return false' > "+category.cy_name+ "</a></li>";
						});
						//console.log(sliSlide);	
					$('#footerProdList').append(footerProdList);
					$('#category-list').append(catList);
					},
					error: function(e)
						{
							alert('AJAX Error!');
							console.log('AJAX Error!');
							console.log(e);
						},
								
			})
			.done(function(){ 
				//Highlight the selected element on products page categories navbar
				$("#hlt").addClass("selected");  
				$('#category-1').addClass('text-white');
				$('a.catLink').click(function(e) {
					$('.catLi.selected').removeClass('selected');
					$('.catLink.text-white').removeClass('text-white');
					var $parent = $(this).parent();
					var $child = $(this).parent().children();
					$parent.addClass('selected');
					$child.addClass('text-white');
					e.preventDefault();
	 			});	
			});
		
		//Highlight the selected element on navbar
		// $('a.barNav').click(function(e){
		// 	$('a.active').removeClass('active text-white');
		// 	$('.liBarNav.selected').removeClass('selected');
		// 	var $parent = $(this).parent();
		// 	var $child = $(this).parent().children();
		// 	$parent.addClass('selected');
		// 	$child.addClass('active text-white');
		// 	e.preventDefault();
		// });

});
</script>