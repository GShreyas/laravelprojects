@include ('header') @include ('navbar')
<!--Technical specifications Modal-->
<div class="modal fade" id="techModal" tabindex="-1" aria-labelledby="techModalLabel" aria-hidden="true">
		  <div class="modal-dialog ">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="techModalLabel">
                    <!-- Variants details here -->
                </h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
              <img
                src="images/techSpec.png"
                alt=""
                class="img-fluid"
               />
			  </div>
			</div>
		  </div>
		</div>

<!--Mechanical specifications Modal-->
<div class="modal fade" id="mechModal" tabindex="-1" aria-labelledby="mechModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="mechModalLabel">
                    <!-- Variants details here -->
                </h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
              <img
                src="images/mechSpec.png"
                alt=""
                class="img-fluid"
               />
			  </div>
			</div>
		  </div>
		</div>        

<!-- Enquiry Modal -->

<div
    class="modal fade"
    id="EnquiryModal"
    tabindex="-1"
    aria-labelledby="EnquiryModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="EnquiryModalLabel">
                    <!-- Variant details -->Enquiry for<br/><span id='VarModName'></span>
                </h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
            </div>
            <div class="modal-body">
                <form method="post" id="enqForm">
                    <div class="input-group visually-hidden" id="hide_inp">
                        <!-- Input field with variant model number and name -->
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control border-primary "
                            placeholder="Enter Name"
                            aria-label="Name"
                            aria-describedby="basic-addon1"
                            id="name"
                            name="name"
                            autofocus
                        />
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="tel"
                            class="form-control border-primary"
                            placeholder="Enter Number"
                            aria-label="Number"
                            aria-describedby="basic-addon2"
                            id="num"
                            name="number"
                            maxlength = "12"
                            
                        />
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="tel"
                            class="form-control border-primary"
                            placeholder="Enter Alternate Number"
                            aria-label="AlternateNumber"
                            aria-describedby="basic-addon4"
                            id="altNum"
                            name="altNumber"
                            maxlength = "12"
                            
                        />
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="mail"
                            class="form-control border-primary"
                            placeholder="Enter E-mail"
                            aria-label="E-mail"
                            aria-describedby="basic-addon3"
                            id="email"
                            name="email"
                        />
                    </div>
                    <div class="input-group">
                        <textarea
                            class="form-control border-primary"
                            placeholder="Enter Enquiry"
                            aria-label="With textarea"
                            id="enq"
                            name="enquiry"
                            maxlength = "500"
                        ></textarea>
                    </div>
                    <div class="d-grid gap-2 col-6 pt-3 mx-auto">
                        <button class="btn btn-primary" type="submit" id="formSubmit">
                            Send via Email
                        </button>
                        <button
                            class="btn btn-success d-block d-sm-none"
                            type="button"
                            href="#"
                            id="wp-button"
                        >
                            Send via Whatsapp
                        </button>
                    </div>  
                </form>
            </div>
        </div>
    </div>
</div>
<!--------------------------------------------------Hero---------------------------------------------------------->
<section class="breadcrumbs mt-0">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2 class="text-white">Products</h2>
          <ol>
            <li><a href="/" class="text-white">Home</a></li>
            <li>Products</li>
          </ol>
        </div>

      </div>
</section><!-- End Breadcrumbs Section -->
<!------------------------------------------------------------------------------------------------------------>
<div class="container">
    <div class="row">
        <div class="col-md-3 pt-3" id="cat_bar" >
            <h2 class="mt-3 fs-5">Categories</h2>
            <ul class="nav flex-column" id="category-list">
            <!-- Category list here -->
            </ul>
        </div>
        <div class="col-md-9 pt-4" id="pro_bar">
          <div id="catSubName">
            <span id="catName" class="fs-5">
                <!-- Category name here -->
            </span>
            <span id="subName" class=" fs-5">
                <!-- Sub Category name here -->
            </span>
            <span id="vrName" class="fs-5">
                <!-- Variants here -->
            </span>
            <span id="vrDisName" class="fs-6">
                <!-- Variant name here -->
            </span>
          </div>  
            <div
                class="row row-cols-1 row-cols-md-3 g-4 py-2"
                id="subCategory-list"
            ></div>
            <div class="row" id="varFill" style="display: none">
                <div class="col-md-6" id="varImg">
                    <!-- Variant display -->
                </div>
                <div class="col-md-6 grid-center">
                    <div class="d-grid gap-2 col-6" id="proBtn">
                        <!-- User manual, Data Sheet & Enquiry Button here -->
                    </div>
                </div>
                <div class="accordion accordion-flush" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button
                                class="accordion-button"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseOne"
                                aria-expanded="true"
                                aria-controls="collapseOne"
                            >
                                Description
                            </button>
                        </h2>
                        <div
                            id="collapseOne"
                            class="accordion-collapse collapse show"
                            aria-labelledby="headingOne"
                            data-bs-parent="#accordionExample"
                        >
                            <div class="accordion-body" id="varDesc">
                                <!-- Variant description here -->
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button
                                class="accordion-button collapsed"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo"
                                aria-expanded="false"
                                aria-controls="collapseTwo"
                            >
                                Features
                            </button>
                        </h2>
                        <div
                            id="collapseTwo"
                            class="accordion-collapse collapse"
                            aria-labelledby="headingTwo"
                            data-bs-parent="#accordionExample"
                        >
                            <div class="accordion-body" >
                             <!-- <a href="#" data-bs-toggle="modal" data-bs-target="#techModal" class="mx-auto">
                                <img
                                    src="images/techSpec.png"
                                    alt=""
                                    class="img-fluid"
                                />
                             </a>     -->
                            <ul id="varFeat">
                                
                            </ul>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button
                                class="accordion-button collapsed"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseThree"
                                aria-expanded="false"
                                aria-controls="collapseThree"
                            >
                                Mechanical Specifications
                            </button>
                        </h2>
                        <div
                            id="collapseThree"
                            class="accordion-collapse collapse"
                            aria-labelledby="headingThree"
                            data-bs-parent="#accordionExample"
                        >
                            <div class="accordion-body">
                            <a href="#" data-bs-toggle="modal" data-bs-target="#mechModal" class="mx-auto">   
                            <img
                                    src="images/mechSpec.png"
                                    alt=""
                                    class="img-fluid"
                                />
                            </a>    
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        //Display category name on opening the page for the first time
        //To display subcategories of the first category when the products page is opened for the first time.
        retrieveSubCat(1, "Solar Charge Controllers");

        //Enquiry form validation
        $.validator.addMethod("alpha", function (value, element) {
            return (
                this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/)
            );
        });

        $.validator.addMethod(
            "phonenu",
            function (value, element) {
                return (
                    this.optional(element) ||
                    /((\+*)((0[ -]+)*|(91 )*)(\d{12}|\d{10}))|\d{5}([- ]*)\d{6}/g.test(
                        value
                    )
                );
            },
            "Invalid phone number"
        );

        $.validator.addMethod(
            "validate_email",
            function (value, element) {
                if (
                    /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    return false;
                }
            },
            "Please enter a valid Email."
        );

        $("#enqForm").validate({
            rules: {
                name: {
                    required: true,
                    alpha: true,
                },
                number: {
                    required: true,
                    phonenu: true,
                },
                altNumber: {
                    required: false,
                    phonenu: true,
                },
                email: {
                    required: true,
                    validate_email: true,
                },
                enquiry:{
                    required: true
                }
            },
            messages: {
                name: "Please enter a valid name",
                number: "Please enter a valid number",
                email: "Please enter a valid email address",
            },
            submitHandler: function (form) {
                var name = $("#name").val();
                var num = $("#num").val();
                var altNum = $("#altNum").val();
                var email = $("#email").val();
                var enq = $("#enq").val();
                var vr_id = $("#var_id").val();
                var var_mod = $("#var_mod").val();
                var formData = {
                    name: name,
                    num: num,
                    altNum: altNum,
                    email: email,
                    enq: enq,
                    vr_id: vr_id,
                    var_mod: var_mod,
                };
                var urls = ["submit-form-db", "submit-form-email"];
                $.each(urls, function (i, u) {
                    $.ajax(u, {
                        type: "POST",
                        data: formData,
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            alert("Success");
                        },
                        error: function (f) {
                            alert("Enquiry form AJAX Error!");
                            console.log("AJAX Error!");
                            console.log(f);
                        },
                    });
                });
                return false;
            },
        });

        $("#wp-button").click(function(){
            if($('#enqForm').valid() == true){
                var name	=	$('#name').val();	
                var num		=	$('#num').val();
                var altNum	=	$('#altNum').val();
                var email	=	$('#email').val();
                var enq	=	$('#enq').val();
                var vr_id = $('#var_id').val();
                var var_mod= $('#var_mod').val();
                var formData = name.concat("|Number:",num,"|Alernate Number:|",altNum,"|E-mail",email,"|Enquiry:",enq,"|Variant ID:",vr_id,"|Variant Model Number:",var_mod);
                window.location.href = 'https://api.whatsapp.com/send?phone=+917899149177&text='+formData; }
            else{
                 alert("Invalid data")
                } 
        });
    });
    //To display sub categories when user selects a category.
    function retrieveSubCat(catID, cyName) {
        $("#vrDisName").hide();
        $("#subName").hide();
        $("#vrName").hide();
        var sendData = { cy_id: catID };
        var catID = catID;
        var catName = cyName;
        $.ajax({
            type: "POST",
            url: "get-category-subCat",
            data: sendData,
            dataType: "json",
            cache: true,
            success: function (response) {
                //console.log(response);
                var allsubCat = "";
                var cyCrumb = "";    
                $.each(response, function (i, subCat) {
                    allsubCat +=
                        "<div class='col'><a href='#' onclick='showProd(\"" +
                        subCat.PK_sub_id +
                        '","' +
                        subCat.sub_name +
                        "\"); return false'><div class='card text-center hover-card shadow'><img src='" +
                        subCat.sub_img +
                        "' class='card-img-top pro-img mx-auto' alt='" +
                        subCat.sub_name +
                        "'><div class='card-body'><h6 class='card-title fs-6'>" +
                        subCat.sub_name +
                        "</h6></div></div></a></div>";
                    cyCrumb = "<a href='#' onclick='retrieveSubCat(\"" +catID+ "\",\"" +cyName + "\"); return false'>"+cyName+"</a>";

                });
                //console.log(allProducts);
                $("#subCategory-list").html(allsubCat);
                $("#catName").html(cyCrumb);
                $("#varFill").hide();
                $("#subCategory-list").show();
            },
            error: function (e) {
                alert("AJAX Error!");
                console.log("AJAX Error!");
                console.log(e);
            },
        });
    }

    //To display products when user selects a sub category
    function showProd(subID, subName) {
        $("#vrDisName").hide();
        $("#subName").show();
        $('#vrName').hide();
        var sendData = { sub_id: subID };
        var subID = subID;
        var subName = subName;
        $.ajax({
            type: "POST",
            url: "get-subcategory-products",
            data: sendData,
            dataType: "json",
            cache: true,
            success: function (response) {
                //console.log(response);
                var subProducts = "";
                var syCrumb = "";
                $.each(response, function (i, product) {
                    subProducts +=
                        "<div class='col'><a href='#' onclick='showVar(" +
                        product.PK_pr_id +
                        ");return false'><div class='card text-center shadow hover-card'><img src='" +
                        product.pr_img +
                        "' class='card-img-top pro-img mx-auto' alt='" +
                        product.pr_name +
                        "'><div class='card-body'><h6 class='card-title fs-6'>" +
                        product.pr_name +
                        "</h6></div></div></a></div>";
                    syCrumb = "<a href='#' onclick='showProd(\"" + subID +'","'+ subName +"\"); return false'>"+subName+"</a>";
                });
                //console.log(allProducts);
                $("#subCategory-list").html(subProducts)
                $("#subName").html(" / " + syCrumb);
                $("#varFill").hide();
                $("#subCategory-list").show();
            },
            error: function (e) {
                alert("AJAX Error!");
                console.log("AJAX Error!");
                console.log(e);
            },
        });
    }
    // To display variants - on clicking view product
    function showVar(prID) {
        $("#vrDisName").hide();
        $('#vrName').show();
        $("#varFill").hide();
        $("#subCategory-list").show();
        var sendData = { prID: prID };
        var prID = prID;
        $.ajax({
            type: "POST",
            url: "get-product-var",
            data: sendData,
            dataType: "json",
            cache: true,
            success: function (response) {
                //console.log(response);
                var variants = "";
                var vrCrumb = "";
                $.each(response, function (i, variant) {
                    variants +=
                        "<div class='col'><a href='#' onclick='disVar(\"" +
                        variant.PK_var_id +
                        '","' +
                        variant.var_mod_no +
                        "\");return false' ><div class='card text-center shadow hover-card'><img src='" +
                        variant.var_img +
                        "' class='card-img-top pro-img mx-auto' alt=''><div class='card-body'><h5 class='card-title fs-6'>" +
                        variant.var_details +
                        "</h5></div></div></a></div>";
                    vrCrumb = "<a href='#' onclick='showVar("+prID+"); return false'>Variants</a>";
                });
                //console.log(variants);
                $("#subCategory-list").html(variants);
                $("#vrName").html(" / " + vrCrumb);
            },
            error: function (e) {
                alert("AJAX Error!");
                console.log("AJAX Error!");
                console.log(e);
            },
        });
    }

    function disVar(varID, varMod) {
        $("#vrDisName").show();
        $("#subCategory-list").hide();
        // $("#varFill").show();
        var sendData = { varID: varID, varMod: varMod };
        var varMod = varMod;
        //console.log(sendData);
        $.ajax({
            type: "POST",
            url: "get-var-details",
            data: sendData,
            dataType: "json",
            cache: true,
            success: function (response) {
                //console.log(response);
                var varImg = "";
                var proBtn = "";
                var varDesc = "";
                var hide_inp = "";
                var VarModName= "";
                var varDisName = "";
                var varFeat = "";
                var splArray = "";
                var feat = "";
                $.each(response, function (i, variant) {
                    varImg +=
                        "<img src='" +
                        variant.var_img +
                        "' alt='' class='img-fluid'/>";
                    proBtn +=
                        "<a class='btn pro-btn mt-0' target='_blank' href='" +
                        variant.vr_ds +
                        "'>Data Sheet</a><a class='btn pro-btn mt-0' target='_blank' href='" +
                        variant.sub_um +
                        "'>User Manual</a><a class='btn pro-btn mt-0' id='enq-btn' data-bs-toggle='modal' data-bs-target='#EnquiryModal' data-bs-dismiss='modal'>Send enquiry</a>";
                    varDesc += "<p>" + variant.sub_desc + "</p>";
                    hide_inp +=
                    "<input type='text' class='form-control' id='var_id' value='"+variant.PK_var_id+"' disabled><input type='text' class='form-control' id='var_mod' value='"+variant.var_mod_no+"-"+variant.var_details+"' disabled>";
                    VarModName += variant.var_details;
                    varFeat = variant.sub_feat;
                    splArray = varFeat.split('*');//splits the text up in chunks
                    //console.log(splArray);
                    $.each(splArray, function(index, item) {
                        feat += "<li>"+item+"</li>";
                        });
                    
                });
                
                //console.log(varImg);
                $("#varImg").html(varImg);
                $("#proBtn").html(proBtn);
                $("#varDesc").html(varDesc);
                $("#varFeat").html(feat);
                $("#hide_inp").html(hide_inp);
                $("#VarModName").html(VarModName);
                $("#vrDisName").html(" / " + VarModName);
                $("#techModalLabel").html(VarModName);
                $("#mechModalLabel").html(VarModName);
                $("#varFill").show();
            },
            error: function (e) {
                alert("AJAX Error!");
                console.log("AJAX Error!");
                console.log(e);
            },
        });
    };

</script>
@include ('footerbar') @include ('footer')
