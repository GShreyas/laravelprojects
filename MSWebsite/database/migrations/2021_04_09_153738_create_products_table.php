<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('PK_pr_id');
			$table->text('pr_name',100);
			$table->text('pr_desc',1000);
			$table->string('pr_img',100);
			$table->string('pr_ds',100);
			$table->string('pr_man',100);
			$table->string('pr_broch',100);
			$table->unsignedInteger('FK_sub_id');
			$table->foreign('FK_sub_id')->references('PK_sub_id')->on('subcategories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
